#===============================================================================
#USAGE : ./ms1-menu.sh orientation
#DESCRIPTION : Opens a terminal menu to execute restart, shutdown, turns-creen, keyboard-language scripts.
#PARAMS: 
#TIPS:
#===============================================================================
#
#!/bin/bash
#START
echo "	Choose the number whose task you want to do
	1. Shutdown
	2. Restart
	3. Turn Screen
	4. Change Keyboard Language"
read input
if [ "$input" == "1" ];
then
	echo "Do you want to shutdown? (Y/N)"
	read inp
	if [ "$inp" = "Y" ];
	then sudo poweroff
	else echo "Not Shutting Down"
	fi
elif [ "$input" == "2" ]
then
	echo "Do you want to restart? (Y/N)"
	read inp
	if [ "$inp" = "Y" ];
	then
		echo "Restarting in"
		for i in {5..1..-1}
			do
				echo $i
				sleep 1
		done
		sudo reboot
	else
	echo "cancelled"
	fi
elif [ "$input" == "3" ]
then
	echo "How do you want to turn the screen? (left/right)"
	read inp
	if [ "$inp" == "left" ]
	then
		echo "Rotating to Left..."
		sleep 1
		xrandr -o left
	elif [ "$inp" == "right" ]
	then
		echo "Rotating to Right..."
		sleep 1
		xrandr -o right
	else
		echo "Invalid input"
	fi
elif [ "input" == "4" ]
then
	read inp
	setxkbmap $inp
	echo "Keyboard language changed to $input"
else
	echo "Invalid input"
fi
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

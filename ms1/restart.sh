#===============================================================================
#USAGE : ./restart.sh
#DESCRIPTION : Restart your own machine.
#TIPS: 
#===============================================================================
#
#!/bin/bash
#START
echo "Do you want to restart? (Y/N)"
read inp
if [ "$inp" = "Y" ];
then
echo "Restarting in"
for i in {5..1..-1}
	do 
		echo $i
		sleep 1
done
sudo reboot
else
echo "cancelled"
fi
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

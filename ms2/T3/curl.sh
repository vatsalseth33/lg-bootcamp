#===============================================================================
#USAGE : ./curl.sh
#DESCRIPTION : A menu to choose between making a http request or download a file using curl. Also to check if domain is up
#PARAMS: No params 
#TIPS: Instead of params, read user input
#===============================================================================
#
#!/bin/bash
#START
echo "Enter a domain name..."
read DomainName
if curl -I $DomainName 2>&1 | grep -w "200\|301" ; then
	echo "Server is up!"
	echo "(1) Make GET request and recieve a JSON
	(2) Download a file from a remote server"
	echo "Enter your choice "
	read input
	if [ $input == "1" ] ; then
	curl -X GET "$DomainName" > get-json-response.json
	elif [ $input == "2" ] ; then
		sudo wget "$DomainName" -O /home/file
		if [[ $file =~ \.gz$ ]];
		then 
			unzip file.zip
		echo "File is located in home folder"
		fi
	fi
else
	echo "Server is down"
	exit 1
fi
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.